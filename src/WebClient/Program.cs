﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WebClient
{
    static class Program
    {
        private static List<string> _firstNames = new List<string> 
        {
            "Alexandr",        
            "Aleksey",
            "Andrey",
            "Jacob",        
            "Jason",
            "Michael",
            "Christo­pher",
            "Ethan",
            "Daniel",
            "Matthew",
            "Andrew",
            "William",
            "Joshua",
            "Isabel­la",
            "Emma",
            "Emi­ly",
            "Sophia",
            "Olivia",
            "Abi­gail",
            "Han­nah",
            "Saman­tha",
            "Mia",
            "Madi­son" 
        };
        private static List<string> _lastNames = new List<string>
        {
            "Smith",
            "John­son",
            "Williams",
            "Brown",
            "Jones",
            "Davis" ,
            "Tay­lor",
            "Wil­son",
            "Robin­son",
            "Wright"
        };

        static HttpClient client = new HttpClient() {};

        static void ConfigureClient()
        {
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }
        static async Task RunAsync()
        {
            ConfigureClient();
            bool state = true;
            while (state)
            {
                try
                {
                    switch (Console.ReadLine().ToUpper())
                    {
                        case "Q":
                            state = false;
                            break;
                        case "GET":
                            Console.Write("Введите ID клиента: ");
                            long id = 0;
                            if (long.TryParse(Console.ReadLine(), out id))
                            {
                                var customer = await GetCustomerAsync(id);
                                if (customer != null)
                                {
                                    Console.WriteLine(customer.ToString());
                                }
                                else
                                {
                                    Console.WriteLine("Клиент с заданым ID не найден!");
                                }
                                    
                            }
                            else
                            {
                                Console.WriteLine("Не корректный ввод ID!");
                            }
                            break;
                        case "CREATE":
                            var customerRequest = RandomCustomer();
                            var response = await CreateCustomerAsync(customerRequest);
                            Console.WriteLine("Команда на создание пользователя ("+ customerRequest.ToString() +") завершена с кодом " + response.StatusCode + " (" +response.ReasonPhrase + ").");
                            break;
                        default:
                            Console.WriteLine("Возможны следующие команды: Q, GET, CREATE");
                            break;
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e.InnerException.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            Random random = new Random();
            return new CustomerCreateRequest(
                _firstNames[random.Next(0,_firstNames.Count)],
                _lastNames[random.Next(0, _lastNames.Count)]);
        }
        static async Task<HttpResponseMessage> CreateCustomerAsync(CustomerCreateRequest customer)
        {


            HttpResponseMessage response = await client.PostAsJsonAsync("customers", customer);
            response.EnsureSuccessStatusCode();
            return response;
        }
        static async Task<Customer> GetCustomerAsync(long id)
        {
            Customer customer = null;
            HttpResponseMessage response = await client.GetAsync("customers\\" + id.ToString());
            if (response.IsSuccessStatusCode)
            {
                customer = await response.Content.ReadFromJsonAsync<Customer>();
            }
            return customer;
        }
    }
}