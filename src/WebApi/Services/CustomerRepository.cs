﻿using System.Collections.Generic;
using WebApi.Models;

namespace WebApi.Services
{
    public class CustomerRepository
    {
        private readonly List<Customer> _customers;
        private long lastId;

        public long LastId { get { return lastId; } }

        public CustomerRepository()
        {
            _customers = new List<Customer>
            {
                new Customer{Id=1, Firstname="Alexandr", Lastname="Kiselev"},
                new Customer{Id=2, Firstname="Aleksey", Lastname="Glushko"},
                new Customer{Id=3, Firstname="Pavel", Lastname="Kantimirov"},
                new Customer{Id=4, Firstname="Maria", Lastname="Esenina"},
                new Customer{Id=5, Firstname="Elena", Lastname="Usova"}
            };  
            lastId=_customers.Count;
        }
        public Customer GetById(long id)
        {
            return _customers.Find(c => c.Id == id);
        }

       public void Add(Customer customer)
        {
            _customers.Add(customer);
            lastId++;
        }

        
    }
}
