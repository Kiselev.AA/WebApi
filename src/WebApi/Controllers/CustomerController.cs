using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private CustomerRepository _customerRepository;
        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]   
        public IActionResult GetCustomerAsync([FromRoute] long id)
        {
            Customer customer=_customerRepository.GetById(id);
            if(customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        [HttpPost("")]   
        public IActionResult CreateCustomerAsync([FromBody] Customer newCustomer)
        {
            var customer = new Customer { Id = _customerRepository.LastId + 1, Firstname = newCustomer.Firstname, Lastname = newCustomer.Lastname };
            if (_customerRepository.GetById(customer.Id) == null)
            {
                _customerRepository.Add(customer);
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }
    }
}